package g30126.futo.henrietta.l7.e2;


	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.Collections;

	import g30126.futo.henrietta.l7.e1.*;

	public class Main {

		public static void main(String[] args) {
			ArrayList<BankAccount> bankAccounts=new ArrayList<>();
			Bank bank=new Bank(bankAccounts);
			bank.addAccounts("ABCD", 50);
			bank.addAccounts("XYZW", 500);
			bank.printAccounts();
			bank.printAccount(30, 100);
			Collections.sort(bank.getAllAccount(),BankAccount.bComparator);
			bank.printAccounts();

		}

	}


