package g30126.futo.henrietta.l4.e6;
import org.junit.Test;
import g30126.futo.henrietta.l4.e4.Author;
import static org.junit.Assert.assertEquals;
public class TestBook2 {
@Test
	public void testTostring() {
		Author[] x=new Author[2];
		x[0]=new Author("Jackson","Jackson25@yahoo.com",'M');
		x[1]=new Author("Richard","Richard22@yahoo.com",'M');
		Book2 b2=new Book2("Maze Runner2",x,1072);
		assertEquals(b2.getName()+" by "+x.length+"authors",b2.toString());
		
	}
@Test
public void TestgetName() {
	Author[] x=new Author[3];
	x[0]=new Author("Jacksonn","Jackson25@yahoo.com",'M');
	x[1]=new Author("Richardd","Richard22@yahoo.com",'M');
	x[2]=new Author("IonCreanga","IonCreanga@yahoo.com",'M');
	Book2 b2=new Book2("Mazee Runner2",x,172);
	assertEquals("Mazee Runner2",b2.getName());
}
@Test
public void TestGetPrice() {
    Author[] x = new Author[3];
    x[0] = new Author("Mihai Eminescu", "ion.creanga@junimea.ro", 'M');
    x[1] = new Author("Ion Creanga", "mihai.eminescu@junimea.ro", 'M');
    x[2] = new Author("Ioan Slavici", "Ioan.Slavici@junimea.ro", 'M');
    Book2 b1 = new Book2("Literatura Romaneasca", x, 10, 50);

    assertEquals(10, b1.getPrice(), 0.01);
}

@Test
public void TestGetQtyInStock() {
    Author[] x = new Author[4];
    x[0] = new Author("Mihai Eminescu", "ion.creanga@junimea.ro", 'M');
    x[1] = new Author("Ion Creanga", "mihai.eminescu@junimea.ro", 'M');
    x[2] = new Author("Ioan Slavici", "Ioan.Slavici@junimea.ro", 'M');
    x[3] = new Author("Camil Petrescu", "Camil.Petrescu@junimea.ro", 'M');
    Book2 b1 = new Book2("Literatura Romaneasca Esentiala", x, 10, 50);

    assertEquals(50, b1.getqtyInStock());
}

@Test
public void TestSetPrice() {
    Author[] x = new Author[2];
    x[0] = new Author("Mihai Eminescu", "ion.creanga@junimea.ro", 'M');
    x[1] = new Author("Ion Creanga", "mihai.eminescu@junimea.ro", 'M');
    Book2 b1 = new Book2("BAC", x, 10, 50);

    b1.setPrice(25);

    assertEquals(25, b1.getPrice(), 0.01);
}

@Test
public void TestSetqtyInStock() {
    Author[] x = new Author[2];
    x[0] = new Author("Mihai Eminescu", "ion.creanga@junimea.ro", 'M');
    x[1] = new Author("Ion Creanga", "mihai.eminescu@junimea.ro", 'M');
    Book2 b1 = new Book2("BAC", x, 10, 50);

    b1.setInstock(1000);
    assertEquals(1000, b1.getqtyInStock(), 0.01);
}

@Test
public void TestPrintAllAuthors() {
    Author[] x = new Author[2];
    x[0] = new Author("Mihai Eminescu", "ion.creanga@junimea.ro", 'M');
   x[1] = new Author("Ion Creanga", "mihai.eminescu@junimea.ro", 'M');
    Book2 b1 = new Book2("BAC", x, 10, 50);

    assertEquals("Mihai Eminescu", x[0].getName());
    assertEquals("Ion Creanga", x[1].getName());

}

}



