package g30126.futo.henrietta.l4.e7;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestCylinder {
	
	    @Test
	    public void TestGetHeight(){
	        Cylinder cylinder = new Cylinder(6,6);
	        assertEquals(6, cylinder.getHeight(),0.01 );

	    }


	    @Test
	    public void TestGetArea(){
	        Cylinder cylinder = new Cylinder(2,3);
	        assertEquals(12.56, cylinder.getArea(),0.01);

	    }

	    @Test
	    public void TestGetVolume(){
	        Cylinder cylinder = new Cylinder(2,3);
	        assertEquals(37.68, cylinder.getVolume(),0.01);
	    }



	}



