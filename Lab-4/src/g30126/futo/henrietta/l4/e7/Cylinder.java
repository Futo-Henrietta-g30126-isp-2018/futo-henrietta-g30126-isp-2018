package g30126.futo.henrietta.l4.e7;

import g30126.futo.henrietta.l4.e3.Circle;

public class Cylinder extends Circle {
	private double height=1.0;
	public Cylinder() {
	}
public Cylinder(double radius) {
	super(radius);
}
public Cylinder(double radius,double height) {
	super(radius);
	this.height=height;
	
}
public double getHeight() {
	return height;
	
}
public double getVolume() {
	 return getArea()*height;
}
public double getArea(){
   return super.getArea();
}
}


