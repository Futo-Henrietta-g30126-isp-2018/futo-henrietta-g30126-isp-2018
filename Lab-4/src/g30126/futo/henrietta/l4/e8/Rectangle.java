package g30126.futo.henrietta.l4.e8;

public class Rectangle extends Shape {
protected double width=1.0;
protected double length=1.0;
public Rectangle(){

}

    public Rectangle(double width, double length){
        this.length=length;
        this.width=width;
}
    public Rectangle(double width, double length, String color, boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
}

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return getLength()*getWidth();
    }
    public double getPerimeter(){
        return (getLength()+getLength())*2;
    }
    @Override
    public String toString(){
        return "A Rectangle with width=" +width+" and length="+length+" , which is a subclass of "+super.toString() ;
    }
}
