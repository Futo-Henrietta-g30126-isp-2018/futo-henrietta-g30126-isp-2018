package g30126.futo.henrietta.l4.e8;
public class Square extends Rectangle {
	  public Square(){

	    }
	    public Square(double side){
	        super.length=side;
	        super.width=side;
	    }
	    public Square(double side, boolean filled, String color){
	        super.length=side;
	        super.width=side;
	        super.color=color;
	        super.filled=filled;

	    }

	    public double getSide(){
	        return length; // return width ( width = lenght ) ;
	    }

	    public void setSide(double side){
	        super.length=side;
	        super.width=side;
	    }
	    public void setWidth(double side){
	        super.length=side;
	        super.width=side;
	    }

	    public String toString(){
	        return "A Square with side="+length+" , which is a subclass of "+super.toString();
	    }
}