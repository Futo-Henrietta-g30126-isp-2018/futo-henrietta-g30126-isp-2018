package g30126.futo.henrietta.l4.e3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCircle {


		@Test
		public void testGetArea() {
		Circle c=new Circle();
		assertEquals(Math.PI*c.getRadius()*c.getRadius(), c.getArea(),0.01);
		}
		@Test
	    public void TestCircleRadius(){
	        Circle c2 = new Circle(10);
	        assertEquals(10,c2.getRadius(),0.01);
	    }

	}
