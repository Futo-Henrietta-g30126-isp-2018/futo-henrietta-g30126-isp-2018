package g30126.futo.henrietta.l4.e4;

 import org.junit.Test;
 import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
public class TestAuthor {
	@Test
	public void testToString () {
		
Author a1= new Author("Pop Dan","popdan@yahoo.com",'m');
assertEquals(a1.getName()+"("+a1.getgender()+")"+" at "+a1.getEmail(),a1.toString());
		

	}
	@Test
	public void testGetName () {
		
    Author a1= new Author("Pop Dan","popdan@yahoo.com",'m');
    assertEquals("Pop Dan",a1.getName());
}
	@Test
	public void testGetEmail () {
	Author a1= new Author("Pop Dan","popdan@yahoo.com",'m');
	assertEquals("popdan@yahoo.com",a1.getEmail());
	}
	@Test
	public void testGetGender () {
	Author a1= new Author("Pop Dan","popdan@yahoo.com",'m');
	assertEquals('m',a1.getgender());
	assertNotNull(a1.getgender());
	}
	@Test
    public void TestSetEmail(){
        Author a1 = new Author("Ion Creanga","ion.creanga@junimea.ro",'M');
        a1.setEmail("office@junimea.ro");
        assertEquals(a1.getEmail(),"office@junimea.ro" );
    }
}




