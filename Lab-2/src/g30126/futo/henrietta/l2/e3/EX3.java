package g30126.futo.henrietta.l2.e3;

import java.util.Scanner;

public class EX3 {
	static int prim(int n) {
		int i,s=0;
		for (i=1;i<=n;i++)
			if (n%i==0)
				s++;
		return s;
	}
	public static void main(String[] args) {
		int a,b,i,d=0;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Capatul din stanga: "); 
		a = in.nextInt();
		
		System.out.println("Capatul din dreapta: "); 
		b = in.nextInt();
		in.close();
		for (i=a; i<=b; i++)
			if(prim(i)==2){
            System.out.println(i);
            d++;
			}
		System.out.println("Numarul de numere prime este:"+d);
			
		
	}

}
