package g30126.futo.henrietta.l2.e6;

import java.util.Scanner;

public class EX6 {
	
		//metoda nerecursiva
	
	static int fact(int n) {
		int i,s=1;
		for(i=1;i<=n;i++)
			s=s*i;
		return s;
	}


		//metode recursiva
	
	static int fact2(int n) {
		if (n==1) return 1;
		else return n*fact2(n-1);
	}

	public static void main(String[] args) {
		int n;
		Scanner in=new Scanner(System.in);
		System.out.println("Dati valoarea lui n=");
		n=in.nextInt();
		in.close();
		System.out.println("Fcatorialul lui n calculat cu metoda recursiva:"+fact2(n));
		System.out.println("Factorialul lui n calculat cu metoda nerecursiva:"+fact(n));
	}

	}



