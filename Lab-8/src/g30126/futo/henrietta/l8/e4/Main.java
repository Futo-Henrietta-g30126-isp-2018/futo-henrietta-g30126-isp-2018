package g30126.futo.henrietta.l8.e4;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws Exception{
		CarFactory carFactory=new CarFactory();
		Car car=carFactory.createCar("Range Rover", 10000);
		Car car2=carFactory.createCar("Audi", 15000);
		carFactory.addCar(car, "car1.dat");
		carFactory.addCar(car2, "car2.dat");
		
		Car car3=carFactory.removeCar("car1.dat");
		System.out.println(car3.toString());
		

	}

}
