package g30126.futo.henrietta.l8.e1;

public class CofeeeDrinker {
	void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException,TooManyCoffeException {
	       if(c.getCount()>5)
	           throw new TooManyCoffeException(c.getCount(),"too many coffes");
	       if(c.getTemp()>60)
	               throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
	         if(c.getConc()>50)
	               throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");        
	         
	             
	         System.out.println("Drink cofee:"+c);
	         
	   }

}

