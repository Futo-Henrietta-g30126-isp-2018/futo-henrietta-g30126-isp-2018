package g30126.futo.henrietta.l3.e6;

public class  MyPoint {
	private int x;
	private int y;


	public MyPoint(){
		x = 0;
		y = 0;
	}
	
	public  MyPoint(int x, int y){
		this.x = x;
		this.y = y;
	}
	

	public int getX(){
		return x;
	}


	public int getY(){
		return y;
	}

	public void setXY(int x, int y){
		this.x = x;
		this.y = y;
	}

	public String toString(){
		return "("+this.x+", "+this.y+")";
	}

	public double distance(int x, int y){
		int xDiff = this.x - x;
		int yDiff = this.y - y;
		return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
	}

	public double distance(MyPoint a){
	   return Math.sqrt((x-a.getX())*(x-a.getX())+(y-a.getY())*(y-a.getY()));
	}
}



