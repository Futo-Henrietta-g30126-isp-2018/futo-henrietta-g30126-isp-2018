package g30126.futo.henrietta.l3.e3;

import becker.robots.*;

public class EX3 {
	  public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	      Robot R07 = new Robot(prague, 1, 1, Direction.NORTH);
	      
	      R07.move();
	      R07.move();
	      R07.move();
	      R07.move();
	      R07.move();
	      R07.turnLeft();	
	      R07.turnLeft();
	      R07.move();
	      R07.move();
	      R07.move();
	      R07.move();
	      R07.move();
	      
}
}
