package g30126.futo.henrietta.l5.e1;


class Circle extends Shape {
    protected double radius;

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return 0; //replace with formula for get area
    }

    public double getPerimeter() {
        return 0; //replace with formula for get perimeter
    }
}
