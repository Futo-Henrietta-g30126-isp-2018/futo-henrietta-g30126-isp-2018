package g30126.futo.henrietta.l6.e1r;

import java.awt.*;

public class Rectangle extends Shape{

    private int x,y,length,width;

    public Rectangle(Color color, int length, int width) {
        super(color);
        this.length = length;
    }
    public int getLength() {
        return length;
    }
   public int getWidth() {
        return width;
    }
    


    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+this.length+" "+getColor().toString());
        g.setColor(getColor());
        if (getfilled()==true) {
        	g.fillRect(getX(), getY(), length, length);
        }
        else
       g.drawRect(getX(), getY(), length, length);
    }
}
